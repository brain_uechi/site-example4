module.exports = {
  syntax: "css",
  extends: ['stylelint-config-recommended'],
  rules: {
    'no-empty-source': null,
    'block-no-empty': null,
    'string-quotes': 'single',
    'no-descending-specificity': null,
    'font-family-no-missing-generic-family-keyword': null,
  },
};
