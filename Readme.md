# example4

## gulp
- browser-sync
- htmlhint
- stylelint

## pre-commit hooks
- husky
    - lint-staged
        - htmlhint
        - stylelint
