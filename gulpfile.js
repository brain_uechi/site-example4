const gulp = require('gulp');
const browserSync = require('browser-sync').create();

/**
 * browser-sync task
 */
gulp.task('serve', function (callback) {
  const config = require('./bs-config');
  browserSync.init(config, () => {
    callback();
  });
});

/**
 * browser-sync reload task
 */
gulp.task('serve:reload', function () {
  return gulp.src([
    'htdocs/**/*.{html,css,js}'
  ], {
    since: gulp.lastRun('serve:reload')
  })
  .pipe(browserSync.stream());
});

/**
 * browser-sync watch task
 */
gulp.task('serve:watch', function (callback) {
  gulp.watch([
    'htdocs/**/*.{html,css,js}'
  ], gulp.series('html:lint', 'css:lint', 'serve:reload'));
  callback();
});

/**
 * html lint task
 */
gulp.task('html:lint', function () {
  const htmlhint = require('gulp-htmlhint');

  return gulp.src([
    'htdocs/**/*.html',
  ], {
    base: 'htdocs',
    since: gulp.lastRun('html:lint'),
  })
  .pipe(htmlhint('.htmlhintrc'))
  .pipe(htmlhint.failAfterError());
});

/**
 * css lint task
 */
gulp.task('css:lint', function () {
  const stylelint = require('gulp-stylelint');

  return gulp.src([
    'htdocs/**/*.css',
  ], {
    base: 'htdocs',
    since: gulp.lastRun('css:lint'),
  })
  .pipe(stylelint({
    failAfterError: true,
    reporters: [
      {
        formatter: 'string',
        console: true,
      },
    ],
  }).on('error', function(){
    let e = new Error('css:lint Error.');
    e.showStack = false;
    throw e;
  }));
});

/**
 * task presets
 */
gulp.task('default', gulp.series('serve:watch', 'serve'));
